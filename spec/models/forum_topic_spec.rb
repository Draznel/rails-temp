require 'forum_topic'
require 'user'
require 'comment'
require 'user_role'

RSpec.describe ForumTopic, type: :model do
  context 'with comments' do
    before(:each) do
      @forum_topic = create(:forum_topic)
      @user = create(:user)
      @other_user = create(:user)
      @comment = create(:comment, user: @user)
    end
    it 'should have no comments on creation' do
      expect(@forum_topic.comments_amount).to eq 0
    end
    it 'should add new comment to the end of the comment list' do
      expect(@comment.forum_topic_id).to_not eq @forum_topic.id
      @forum_topic.add_comment(@comment)

      expect(@forum_topic.comments)
        .to satisfy { |comments| comments[-1] == @comment }
    end
    it 'should remove a comment' do
      expect do
        @forum_topic.add_comment(@comment)
        @forum_topic.delete_comment(@comment.id, @user)
      end.not_to change { @forum_topic.comments_amount }
    end
    it 'should not remove a comment if it is not applicable for deletion' do
      allow(Comment).to receive(:find).with(@comment.id).and_return(@comment)
      allow(@comment).to receive(:can_delete?).with(@user).and_return(false)
      comments_stub = double
      allow(@forum_topic).to receive(:comments).and_return(comments_stub)
      expect(comments_stub).not_to receive(:destroy)

      @forum_topic.delete_comment(@comment.id, @user)
    end
    it "should change comment's forum_topic_id to the adding one" do
      comment1 = create(:comment)
      @forum_topic.add_comment(comment1)
      expect(comment1.forum_topic_id).to eq @forum_topic.id
    end
    it 'should not allow user to remove other user\'s comment' do
      @forum_topic.add_comment(@comment)
      @forum_topic.delete_comment(@comment.id, @other_user)

      expect(@forum_topic.comments)
        .to contain_exactly(@comment)
    end
    it 'should allow MOD to remove other user\'s comment' do
      @other_user.change_role UserRole::MOD
      expect do
        @forum_topic.add_comment(@comment)
        @forum_topic.delete_comment(@comment.id, @other_user)
      end.not_to change { @forum_topic.comments_amount }
    end
    it 'should allow ADMIN to remove other user\'s comment' do
      @other_user.change_role UserRole::ADMIN

      @forum_topic.add_comment(@comment)
      @forum_topic.delete_comment(@comment.id, @other_user)

      expect(@forum_topic.comments).to match_array([])
    end
  end

  context 'with votes'do
    before(:each) do
      @forum_topic = create(:forum_topic)
      @user0 = create(:user)
      @user1 = create(:user)
      @user2 = create(:user)
    end
    it 'should have no votes on creation' do
      expect(@forum_topic.total_votes).to eq 0
    end
    it 'should add a correct upvote' do
      @forum_topic.upvote(@user0)
      last_vote = @forum_topic.vote_list[-1]
      expect(Vote.valid_type last_vote.vote_type).to eq true
    end
    it 'should add a correct downvote' do
      @forum_topic.downvote(@user0)
      last_vote = @forum_topic.vote_list[-1]
      expect(Vote.valid_type last_vote.vote_type).to eq true
    end
    it 'should count votes' do
      expect do
        @forum_topic.upvote(@user0)
        @forum_topic.downvote(@user0)
        @forum_topic.downvote(@user1)
        @forum_topic.upvote(@user2)
      end.to change { @forum_topic.total_votes }.by(3)
    end
    it 'should count upvotes' do
      expect do
        @forum_topic.upvote(@user0)
        @forum_topic.upvote(@user1)
      end.to change { @forum_topic.votes_diff }.by(2)
    end
    it 'should count downvotes' do
      expect do
        @forum_topic.downvote(@user0)
        @forum_topic.downvote(@user1)
      end.to change { @forum_topic.votes_diff }.by(-2)
    end
    it 'should show correct amount of upvotes' do
      @forum_topic.upvote(@user0)
      @forum_topic.upvote(@user1)
      @forum_topic.downvote(@user0)

      expect(@forum_topic.upvotes).to eq 1
    end
    it 'should show correct amount of upvotes' do
      @forum_topic.downvote(@user0)
      @forum_topic.downvote(@user0)
      @forum_topic.downvote(@user1)

      expect(@forum_topic.downvotes).to eq 2
    end
    it 'should rewrite older vote if user already voted' do
      vote = spy('vote')
      allow(@forum_topic).to receive(:contains_vote?).with(@user0)
        .and_return(true)
      allow(@forum_topic).to receive(:user_vote).with(@user0).and_return(vote)
      allow(@forum_topic).to receive(:update_attributes).and_return(true)

      @forum_topic.downvote(@user0)
      expect(vote).to have_received(:change_type)
    end
    it 'should rewrite older votes of multiple users' do
      expect do
        @forum_topic.upvote(@user0)
        @forum_topic.upvote(@user1)
        @forum_topic.downvote(@user0)
      end.not_to change { @forum_topic.votes_diff }
    end
    it "should contain user's vote" do
      @forum_topic.upvote(@user0)
      expect(@forum_topic.contains_vote? @user0).to be_truthy
    end
    it "should not contain user's vote" do
      @forum_topic.upvote(@user0)
      expect(@forum_topic.contains_vote? @user1).to be_falsey
    end
  end
end
