require './app/models/comment'
require 'user'

RSpec.describe Comment, type: :model do
  context 'general' do
    before(:each) do
      @user0 = create(:user)
      @user1 = create(:user)
      @comment = create(:comment, user: @user0)
    end
    it 'should have same author' do
      expect(@comment.same_author?(@user0)).to be_truthy
    end
    it 'should not have same author' do
      expect(@comment.same_author?(@user1)).to be_falsy
    end
    it 'should allow same author to delete comment' do
      expect(@comment.can_delete?(@user0)).to be_truthy
    end
    it 'should not allow user to delete not his own comment' do
      expect(@comment.can_delete?(@user1)).to be_falsy
    end
    it 'should allow a powered role to delete not his own comment' do
      @user1.change_role('MOD')
      expect(@comment.can_delete?(@user1)).to be_truthy
    end
  end
end
