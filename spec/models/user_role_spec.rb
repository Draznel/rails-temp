require './app/models/user_role'

RSpec.describe UserRole, type: :model do
  context 'powered' do
    it 'user should not be a powered role' do
      expect(UserRole.powered? UserRole::USER).to eq false
    end
    it 'mod should be a powered role' do
      expect(UserRole.powered? UserRole::MOD).to eq true
    end
    it 'admin should be a powered role' do
      expect(UserRole.powered? UserRole::ADMIN).to eq true
    end
  end
  context 'valid' do
    it 'should be a valid role' do
      expect(UserRole.valid? UserRole::USER).to eq true
    end
    it 'string should be valid as a role' do
      expect(UserRole.valid? 'MOD').to eq true
    end
    it 'should not be valid' do
      expect(UserRole.valid? 'foo').to eq false
    end
  end
end
