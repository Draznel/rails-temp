require 'rails_helper'
require 'user'
require 'user_role'

RSpec.describe User, type: :model do
  context 'creation' do
    it 'should correctly create different users' do
      user1 = create(:user)
      user2 = create(:user)

      expect(user1).not_to eq user2
    end
  end
  context 'general' do
    before(:each) do
      @user = create(:user, display_name: 'username', email: 'foo@bar.com')
    end
    it "should change user's display name" do
      @user.change_name 'foo'
      expect(@user.display_name).to eq 'foo'
    end
    it "should change user's password" do
      @user.change_password 'secret'
      expect(@user.password).to eq 'secret'
    end
    it "should change user's email" do
      @user.change_email 'test@test.com'
      expect(@user.email).to eq 'test@test.com'
    end
    it "should not change user's email to an invalid one" do
      @user.change_email 'invalid_email.com'
      expect(@user.email).to eq 'foo@bar.com'
    end
  end
end
