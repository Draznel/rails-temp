FactoryGirl.define do
  factory :user, class: User do
    sequence(:display_name, 10) { |n| "TestUser#{n}" }
    password 'secret'
    sequence(:email, 10) { |n| "email#{n}@foo.com" }
    role 'USER'
  end
  factory :user2, class: User do
    sequence(:display_name, 10) { |n| "TestUser2#{n}" }
    password 'secret2'
    sequence(:email, 10) { |n| "email#{n}@foo2.com" }
    role 'USER'
  end
end
