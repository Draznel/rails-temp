FactoryGirl.define do
  factory :upvote, class: Vote do
    user
    forum_topic
    vote_type '+'
  end
  factory :downvote, class: Vote do
    user
    forum_topic
    vote_type '-'
  end
end
