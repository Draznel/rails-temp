FactoryGirl.define do
  factory :forum_topic, class: ForumTopic do
    sequence(:title, 10) { |n| "Test Topic \##{n}" }
    forum_section
    user
  end
  factory :empty_forum_topic, class: ForumTopic do
    sequence(:title, 10) { |n| "Empty Test Topic \##{n}" }
    forum_section
    user2
  end
end
