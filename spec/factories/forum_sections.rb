FactoryGirl.define do
  factory :forum_section, class: ForumSection do
    sequence(:title, 10) { |n| "Test Section \##{n}" }
    minimum_required_role 'USER'
  end
end
