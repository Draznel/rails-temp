FactoryGirl.define do
  factory :comment, class: Comment do
    user
    forum_topic
    contents 'Hello'
  end
end
