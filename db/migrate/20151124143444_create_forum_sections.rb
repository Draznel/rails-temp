class CreateForumSections < ActiveRecord::Migration
  def change
    create_table :forum_sections do |t|
      t.string :title, null: false
      t.string :minimum_required_role, null: false

      t.timestamps null: false
    end
  end
end
