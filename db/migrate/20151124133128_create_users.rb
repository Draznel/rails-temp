class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :display_name, null: false
      t.string :password, null: false
      t.string :email
      t.string :role, null: false

      t.timestamps null: false
    end
  end
end
