class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.references :user, foreign_key: true, null: false
      t.references :forum_topic, foreign_key: true, null: false
      t.string :vote_type, null: false

      t.timestamps null: false
    end
    add_index :votes, [:user_id, :forum_topic_id], unique: true
  end
end
