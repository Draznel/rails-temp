class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer :thread_id
      t.string :topic
      t.integer :sender, foreign_key: true, null: false
      t.integer :recipient, foreign_key: true, null: false
      t.text :contents
      t.boolean :is_read, default: false

      t.timestamps null: false
    end
    add_reference :messages, :sender, references: :users, index: true
    add_foreign_key :messages, :users, column: :sender_id

    add_reference :messages, :recipient, references: :users, index: true
    add_foreign_key :messages, :users, column: :recipient_id
  end
end
