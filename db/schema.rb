# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151129133112) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "comments", force: :cascade do |t|
    t.integer  "user_id",        null: false
    t.integer  "forum_topic_id", null: false
    t.text     "contents",       null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "comments", ["forum_topic_id"], name: "index_comments_on_forum_topic_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "forum_sections", force: :cascade do |t|
    t.string   "title",                 null: false
    t.string   "minimum_required_role", null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "forum_topics", force: :cascade do |t|
    t.string   "title",            null: false
    t.integer  "forum_section_id", null: false
    t.integer  "user_id",          null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "forum_topics", ["forum_section_id"], name: "index_forum_topics_on_forum_section_id", using: :btree
  add_index "forum_topics", ["user_id"], name: "index_forum_topics_on_user_id", using: :btree

  create_table "messages", force: :cascade do |t|
    t.integer  "thread_id"
    t.string   "topic"
    t.integer  "sender",                       null: false
    t.integer  "recipient",                    null: false
    t.text     "contents"
    t.boolean  "is_read",      default: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "sender_id"
    t.integer  "recipient_id"
  end

  add_index "messages", ["recipient_id"], name: "index_messages_on_recipient_id", using: :btree
  add_index "messages", ["sender_id"], name: "index_messages_on_sender_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "display_name", null: false
    t.string   "password",     null: false
    t.string   "email"
    t.string   "role",         null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "votes", force: :cascade do |t|
    t.integer  "user_id",        null: false
    t.integer  "forum_topic_id", null: false
    t.string   "vote_type",      null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "votes", ["user_id", "forum_topic_id"], name: "index_votes_on_user_id_and_forum_topic_id", unique: true, using: :btree

  add_foreign_key "comments", "forum_topics"
  add_foreign_key "comments", "users"
  add_foreign_key "forum_topics", "forum_sections"
  add_foreign_key "forum_topics", "users"
  add_foreign_key "messages", "users", column: "recipient_id"
  add_foreign_key "messages", "users", column: "sender_id"
  add_foreign_key "votes", "forum_topics"
  add_foreign_key "votes", "users"
end
