# a Comment class
class Comment < ActiveRecord::Base
  belongs_to :forum_topic
  belongs_to :user

  validates :contents, presence: true, length: { maximum: 300 }

  def can_delete?(user)
    same_author?(user) || UserRole.powered?(user.role)
  end

  def same_author?(user)
    self[:user_id] == user.id
  end
end
