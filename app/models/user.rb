require 'user_role'

# a User class
class User < ActiveRecord::Base
  VALID_EMAIL_REGEX = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

  has_many :forum_topics
  has_many :comments, through: :forum_topics
  has_many :votes, through: :forum_topics
  has_many :messages

  validates :display_name, presence: true, uniqueness: true,
                           length: { minimum: 3, maximum: 20 }
  validates :password, presence: true,
                       length: { minimum: 3, maximum: 20 }
  validates :email, uniqueness: true,
                    format: { with: VALID_EMAIL_REGEX, on: :create }
  validates :role, inclusion: %w(USER MOD ADMIN)

  def display_name
    self[:display_name]
  end

  def password
    self[:password]
  end

  def email
    self[:email]
  end

  def role
    self[:role]
  end

  def change_name(name)
    self[:display_name] = name
    save
  end

  def change_password(password)
    self[:password] = password
    save
  end

  def change_email(email)
    return unless email =~ VALID_EMAIL_REGEX
    self[:email] = email
    save
  end

  def change_role(role)
    return unless UserRole.valid? role
    self[:role] = role
    save
  end

  def ==(other)
    state == other.state
  end

  def state
    [self[:id], display_name, password,
     email, role, self[:created_at], self[:updated_at]]
  end
end
