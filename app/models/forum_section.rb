# a ForumSection class
class ForumSection < ActiveRecord::Base
  has_many :forum_topics, dependent: :destroy

  validates :title, presence: true, length: { minimum: 5, maximum: 40 }
  validates :minimum_required_role, inclusion: %w(USER MOD ADMIN)
end
