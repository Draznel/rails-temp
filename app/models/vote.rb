# a Vote class
class Vote < ActiveRecord::Base
  belongs_to :forum_topic
  belongs_to :user

  validates :vote_type, presence: true, inclusion: %w(+ -), length: { is: 1 }

  def change_type(vote_type)
    update_attributes vote_type: vote_type if Vote.valid_type vote_type
    save
  end

  def self.valid_type(vote_type)
    vote_type == '+' || vote_type == '-'
  end
end
