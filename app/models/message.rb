# a Message class
class Message < ActiveRecord::Base
  belongs_to :sender, class_name: 'User'
  belongs_to :recipient, class_name: 'User'

  validates :thread_id, presence: true
  validates :topic, presence: true, length: { maximum: 30 }
  validates :contents, presence: true, length: { maximum: 180 }
end
