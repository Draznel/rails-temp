# a ForumTopic class
class ForumTopic < ActiveRecord::Base
  belongs_to :forum_section
  belongs_to :user
  has_many :comments, dependent: :destroy
  has_many :votes, dependent: :destroy

  validates :title, presence: true, length: { minimum: 5, maximum: 40 }

  def add_comment(comment)
    update_attributes comments: comments.push(comment)
  end

  def delete_comment(comment_id, user)
    comment = Comment.find(comment_id)
    comments.destroy(comment) if comment.can_delete?(user)
    save
  end

  def comments_amount
    (comments || []).size
  end

  def votes_diff
    upvotes - downvotes
  end

  def vote_list
    votes || []
  end

  def total_votes
    vote_list.size
  end

  def upvotes
    values = Vote.where('forum_topic_id = ? and vote_type = ?',
                        self[:id].to_s, '+')
    (values || []).size
  end

  def downvotes
    values = Vote.where('forum_topic_id = ? and vote_type = ?',
                        self[:id].to_s, '-')
    (values || []).size
  end

  def upvote(user)
    vote(user: user, vote_type: '+')
  end

  def downvote(user)
    vote(user: user, vote_type: '-')
  end

  def vote(vote_opts)
    if contains_vote?(vote_opts[:user])
      update_existing_vote(vote_opts)
    else
      add_new_vote(vote_opts)
    end
  end

  def update_existing_vote(vote_opts)
    user = vote_opts[:user]
    return unless contains_vote? user
    vote = user_vote user
    vote.change_type vote_opts[:vote_type] if vote
  end

  def add_new_vote(vote_opts)
    vote_type = vote_opts[:vote_type]
    return unless Vote.valid_type vote_type
    vote_obj = Vote.create(user: vote_opts[:user], forum_topic: self,
                           vote_type: vote_type)
    update_attributes votes: votes.push(vote_obj)
  end

  def contains_vote?(user)
    Vote.where('forum_topic_id = ? and user_id = ?',
               self[:id].to_s, user.id).size > 0
  end

  def user_vote(user)
    Vote.where('forum_topic_id = ? and user_id = ?',
               self[:id].to_s, user.id)[0]
  end
end
