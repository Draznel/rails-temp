class ForumSectionsController < ApplicationController
  before_action :set_forum_section, only: [:show, :edit, :update, :destroy]

  # GET /forum_sections
  # GET /forum_sections.json
  def index
    @forum_sections = ForumSection.all
  end

  # GET /forum_sections/1
  # GET /forum_sections/1.json
  def show
  end

  # GET /forum_sections/new
  def new
    @forum_section = ForumSection.new
  end

  # GET /forum_sections/1/edit
  def edit
  end

  # POST /forum_sections
  # POST /forum_sections.json
  def create
    @forum_section = ForumSection.new(forum_section_params)

    respond_to do |format|
      if @forum_section.save
        format.html { redirect_to @forum_section, notice: 'Forum section was successfully created.' }
        format.json { render :show, status: :created, location: @forum_section }
      else
        format.html { render :new }
        format.json { render json: @forum_section.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /forum_sections/1
  # PATCH/PUT /forum_sections/1.json
  def update
    respond_to do |format|
      if @forum_section.update(forum_section_params)
        format.html { redirect_to @forum_section, notice: 'Forum section was successfully updated.' }
        format.json { render :show, status: :ok, location: @forum_section }
      else
        format.html { render :edit }
        format.json { render json: @forum_section.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /forum_sections/1
  # DELETE /forum_sections/1.json
  def destroy
    @forum_section.destroy
    respond_to do |format|
      format.html { redirect_to forum_sections_url, notice: 'Forum section was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_forum_section
      @forum_section = ForumSection.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def forum_section_params
      params.require(:forum_section).permit(:title, :minimum_required_role)
    end
end
