class CommentsController < ApplicationController
  def create
    @forum_topic = ForumTopic.find(params[:forum_topic_id])

    @comment = @forum_topic.comments.create(comment_params)

    redirect_to forum_topic_path(@forum_topic)
  end

  private
    def comment_params
      params.require(:comment).permit(:author_id, :contents)
    end
end
