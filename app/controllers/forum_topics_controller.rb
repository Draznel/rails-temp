class ForumTopicsController < ApplicationController
  before_action :set_forum_topic, only: [:show, :edit, :update, :destroy]

  # GET /forum_topics
  # GET /forum_topics.json
  def index
    @forum_topics = ForumTopic.all
  end

  # GET /forum_topics/1
  # GET /forum_topics/1.json
  def show
  end

  # GET /forum_topics/new
  def new
    @forum_topic = ForumTopic.new
  end

  # GET /forum_topics/1/edit
  def edit
  end

  # POST /forum_topics
  # POST /forum_topics.json
  def create
    @forum_topic = ForumTopic.new(forum_topic_params)

    respond_to do |format|
      if @forum_topic.save
        format.html { redirect_to @forum_topic, notice: 'Forum topic was successfully created.' }
        format.json { render :show, status: :created, location: @forum_topic }
      else
        format.html { render :new }
        format.json { render json: @forum_topic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /forum_topics/1
  # PATCH/PUT /forum_topics/1.json
  def update
    respond_to do |format|
      if @forum_topic.update(forum_topic_params)
        format.html { redirect_to forum_topic_path(@forum_topic), notice: 'Forum topic was successfully updated.' }
        format.json { render :show, status: :ok, location: @forum_topic }
      else
        format.html { render :edit }
        format.json { render json: @forum_topic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /forum_topics/1
  # DELETE /forum_topics/1.json
  def destroy
    @forum_topic.destroy
    respond_to do |format|
      format.html { redirect_to forum_topics_url, notice: 'Forum topic was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_forum_topic
      @forum_topic = ForumTopic.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def forum_topic_params
      params.require(:forum_topic).permit(:title, :forum_section_id, :creator_id)
    end
end
