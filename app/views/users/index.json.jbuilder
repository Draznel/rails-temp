json.array!(@users) do |user|
  json.extract! user, :id, :display_name, :password, :email, :role
  json.url user_url(user, format: :json)
end
