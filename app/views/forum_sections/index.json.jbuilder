json.array!(@forum_sections) do |forum_section|
  json.extract! forum_section, :id, :title, :minimum_required_role
  json.url forum_section_url(forum_section, format: :json)
end
