json.array!(@messages) do |message|
  json.extract! message, :id, :thread_id, :topic, :sender_id, :recipient_id, :contents, :is_read
  json.url message_url(message, format: :json)
end
