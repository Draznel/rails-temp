json.extract! @message, :id, :thread_id, :topic, :sender_id, :recipient_id, :contents, :is_read, :created_at, :updated_at
